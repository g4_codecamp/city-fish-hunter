<?php
	session_start();
	
	require_once 'config.php';
	require_once 'auto_expire.php';
	require_once 'sidebar.php';
	
	$db_handle = new DBController();
	$username = $_SESSION['Username'];
	$userID = $_SESSION['PersonID'];
	
	if(isset($_POST['submit'])){
		
		$feedback = $_POST['feedback'];
		sanitise($db_handle->connectDB(),$feedback);
		
		$db_handle->addFeedback($userID,$username,$feedback);
	}
	
	function sanitise($conn,$string){
		return htmlentities(mysql_fix_string($conn,$string));
	}
	
	function mysql_fix_string($conn, $string){
		if(get_magic_quotes_gpc()) $string = stripslashes($string);
		return $conn->real_escape_string($string);
	}
?>

<html>
<link rel="stylesheet" type="text/css" href="cityfishhunter.css">
<body>
	<div class="topnav">
        <div class="content">
			<a class="sidebar" onclick="w3_open()">☰</a>					
            <a href="cart.php" style="float: right;">Cart</a>
        </div>
    </div>
    <div class="container">
	<div class="feedbackbox">
		<h2>What Can We Improve?</h2>

		<form action="feedbackpage.php" method="POST">
		  <textarea name="feedback" rows="25" cols="100" maxlength="300" placeholder="Give us some feedback....." style="resize:none"></textarea><br>
		  <?php if(!isset($_POST['submit'])){
			  echo '<input type="submit" value="Submit" name="submit">';
		  }?>
		</form> 
	</div>
	</div>


</body>
</html>