<?php
	session_start();
	
	require_once 'auto_expire.php';
	require_once 'sidebar.php'
?>

<html>
<link rel="stylesheet" type="text/css" href="cityfishhunter.css">
<body>
	<div class="topnav">
        <div class="content">
			<a class="sidebar" onclick="w3_open()">☰</a>					
            <a href="cart.php" style="float: right;">Cart</a>
        </div>
    </div>
    <div class="container">
	<h2 style="color:white; text-align:center">About Us</h2>
	<div class="aboutUsBox">
		<p>City Fish Hunter is a Malaysia aquatic products company. It has 2 shops in Malaysia, which are located at Kuala Lumpur and Subang Jaya. 
		City Fish Hunter was founded in 2008. The founder of the company wishes that city folks can have a chance to taste the “Village Freshness Seafood”. 
		Therefore, he started co-operating with the fishermen in selling fresh catch seafood to cities.<br>
		The aim of City Fish Hunter is to provide the freshest seafood in the city to the customer, bypassing wholesalers, and grocery stores. 
		The products are from Port Dickson and Sekinchan jetties, which are famous for seafood wholesale. All the freshest catch will be professionally processed without
		adding any chemical and preservatives contamination. This ensures customers receive clean and healthy seafood with reasonable pricing and peace of mind.
		</p>
	</div>
	</div>


</body>
</html>