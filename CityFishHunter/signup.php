<?php
	session_start();
	
	require_once 'config.php';
	$db_handle = new DBController();
	$confirmPassword = $username = $email = $password = "";
	$msg="";
	
	if(isset($_POST['signup'])){
		
		if(!empty($_POST['username']) && !empty($_POST['email']) && !empty($_POST['password']) && ($_POST['password'] == $_POST['confirmPassword'])){
			
			
			$validation = data_validation($_POST['username'], "/^[a-z\d_]{5,20}$/" , "username");
			$validation .= data_validation($_POST['email'],  "/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/" , "email");
			$validation .= data_validation($_POST['password'], '/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,12}$/', "password - at least one letter, at least one number, and there have to be 6-12 characters");
			$msg = $validation;
			if($validation == ""){
				$username 	= sanitise($db_handle->connectDB(),$_POST['username']);
				$email 	= sanitise($db_handle->connectDB(),$_POST['email']);
				$password 	= sanitise($db_handle->connectDB(),$_POST['password']);
				
				$hash_password = password_hash($password,PASSWORD_DEFAULT);
				
				$getNumRows = $db_handle->numRows("select * from users where Username='".$username."'");
				
				if($getNumRows == 0)
				{
					$db_handle->signup($username,$hash_password);
					$msg = "Sign up successfully.<br>
					Please <a href=index.php>Click Here</a> to log in.";
				}
				else
				{
					$msg = "This username had been used.";
				}
			}
		}else{
			$msg = "Make sure you have filled in all the details properly.";
		}
	}
	
	function sanitise($conn,$string){
		return htmlentities(mysql_fix_string($conn,$string));
	}
	
	function mysql_fix_string($conn, $string){
		if(get_magic_quotes_gpc()) $string = stripslashes($string);
		return $conn->real_escape_string($string);
	}		
	
	
	function data_validation($data, $data_pattern, $data_type )
	  {
		if (preg_match($data_pattern, $data)) {
		return "";
		} else {
		return " Invalid data for " .  $data_type . ";";
		}  
	  }   
?>

<html>
    <head>
        <meta charset="utf-8">
		<script>
		function validate(form){
			fail = validateUsername(form.username.value)
			fail += validateEmail(form.email.value)
			fail += validatePassword(form.password.value)
			fail += validateConfirmPassword(form.confirmPassword.value, form.password.value)
			
			if(fail == "")	return true;
			else{ alert(fail); return false }
		}
		
		function validateUsername(field){
			return (field == "") ? "No Forename was entered.\\n" : ""
		}
		
		function validateEmail(field){
			if(field == "") return "No Email was entered. \\n" ;
				else if(!((field.indexOf(".") > 0) && (field.indexOf("@") > 0)) ||
				/[^a-zA-Z0-9.@_-]/.test(field))
					return "The Email Address is Invalid.\\n"
			return ""
		}
		
		function validatePassword(field){
			if(field == ""){
				return "No Password was entered. \\n" ;
			}else if(field.length < 6){
				return "Password must be at least 6 characters.\\n";
			}else if(!(/[a-z]/.test(field) || /[A-Z]/.test(field)) || !/[0-9]/.test(field)){
				return "Password must contain one each of aA-zZ and 0-9. \\n" ;
			}else{
				return "";
			}
		}
		
		function validateConfirmPassword(field,field2){
			if(field != field2){
				return "Confirm Password must be the same as Password";
			}else{
				return "";
			}
		}
		</script>
    </head>
    <link rel="stylesheet" href="login.css" type="text/css">
<body>
<div class="container">
<form action="signup.php" method="POST" onsubmit='return validate(this)'>
  <h1>Sign Up</h1>
  <label for="username">Username:</label><br>
  <input type="text" maxlength="32" value=<?php echo"'".$username."'"?> placeholder="Enter Username" name="username" required><br><br>
  
  <label for="username">Email:</label><br>
  <input type="text" maxlength="40" value=<?php echo"'".$email."'"?> placeholder="Enter Email" name="email" required><br><br>

  <label for="password">Password:</label>
  <input type="password" maxlength="25" value=<?php echo"'".$password."'"?> placeholder="Enter Password" name="password" required><br><br>
  
  <label for="password">Re-enter Password:</label>
  <input type="password" maxlength="25" value=<?php echo"'".$confirmPassword."'"?> placeholder="Enter Password" name="confirmPassword" required>
  <div style="color:red;"><?php echo $msg ?></div><br>

  <input type="submit" name="signup" value="Sign Up">
  <a href="index.php">Already have an account?</a>
</form>
</div>

<?php
$msg="";
?>
</body>
</html>