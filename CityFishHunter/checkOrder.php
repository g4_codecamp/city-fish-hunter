<?php
	session_start();
	
	require_once 'config.php';
	require_once 'auto_expire.php';
	$db_handle = new DBController();
	require_once 'sidebar.php';
	
	$query = "SELECT * FROM orders WHERE PersonID='".$_SESSION['PersonID']."'";
	$result = $db_handle->runQuery($query);
?>

<html>
<link rel="stylesheet" type="text/css" href="cityfishhunter.css">
<body>
	<div class="topnav">
        <div class="content">
			<a class="sidebar" onclick="w3_open()">☰</a>					
            <a href="cart.php" style="float: right;">Cart</a>
        </div>
    </div>
    <div class="container">
		<div class="contentProduct">
				<h1>Order History</h1>
				<div class="grid-container-orders" >
				<?php
				if (!empty($result)) { 
					foreach($result as $key=>$value){
						$number = 1;
						$items = unserialize($result[$key]['ItemsPurchased']);
				?>
						<div class="grid-item">  
							<?php 
							echo "Order ID:".$result[$key]['orderID']."<br>";
							echo "Date:".$result[$key]['CheckOutDate']."<br>";
							echo "Total Price:".$result[$key]['TotalPrice']."<br>";?>
							<table style="margin:auto">
							  <tr>
								<th>NO.</th>
								<th>Product Name</th>
								<th>Quantity</th>
								<th>Price per Unit</th>
							  </tr>
							  
						<?php
						foreach($items as $key=>$value){
						?>
							       
								  <tr>
									<td><?php echo $number ?></td>
									<td><?php echo $items[$key]['name'] ?></td>
									<td><?php echo $items[$key]['quantity']?></td>
									<td><?php echo $items[$key]['price'] ?></td>
								  </tr>
						<?php
						$number += 1;
						}
						?>
						</table>

						</div>
				<?php
						$items = [];
					}
				}
				?>
				</div>
	</div>
	</div>


</body>
</html>