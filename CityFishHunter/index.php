<?php
	session_start();
	
	require_once 'config.php';
	require_once 'auto_expire.php';
	$db_handle = new DBController();
	$errorMsg="";
	
	if(isset($_POST['login'])){
		
		if(!empty($_POST['username']) && !empty($_POST['password']))
		{
			$username 	= sanitise($db_handle->connectDB(),$_POST['username']);
			$password 	= sanitise($db_handle->connectDB(),$_POST['password']);
			
			if($userRow = $db_handle->login($username,$password)){
				if(password_verify($password,$userRow['Password']))
				{
					unset($userRow['password']);
					
					$_SESSION = $userRow;
					header('location:homepage.php');
					exit;
				}
				else
				{
					$errorMsg = "Wrong password";
					
				}
			}else{
				echo "Username is not found or Incorrect Password";
			}
		}
	}
	
	function sanitise($conn,$string){
		return htmlentities(mysql_fix_string($conn,$string));
	}
	
	function mysql_fix_string($conn, $string){
		if(get_magic_quotes_gpc()) $string = stripslashes($string);
		return $conn->real_escape_string($string);
	}		
?>

<html>
    <head>
        <meta charset="utf-8">
    </head>
    <link rel="stylesheet" href="login.css" type="text/css">
<body>

<div class="container">
<form action="index.php" method="POST">
  <h1>Login Here</h1>
  <label for="username">Username:</label><br>
  <input type="text" placeholder= "Enter Username" name="username" required><br><br>

  <label for="password">Password:</label>
  <input type="password" placeholder="Enter Password" name="password" required>
  <div style="color:red;"><?php echo $errorMsg;?></div><br>

  <input type="submit" name="login" value="Login">
  <a href="signup.php">Don't have an account?</a>
</form>
</div>

<?php
$errorMsg="";
?>
</body>
</html>