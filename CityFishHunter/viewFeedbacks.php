<?php
	session_start();

	require_once 'config.php';
	require_once 'auto_expire.php';
	require_once 'sidebar.php';
	$db_handle = new DBController();
	
?>

<html>
<link rel="stylesheet" type="text/css" href="cityfishhunter.css">
<body>
	<div class="topnav">
        <div class="content">
			<a class="sidebar" onclick="w3_open()">☰</a>					
            <a href="cart.php" style="float: right;">Cart</a>
        </div>
    </div>
    <div class="container">
	<div class="viewFeedbackbox">
		<h2>Feedbacks</h2>
		<table>
			  <tr>
				<th>ID</th>
				<th>Username</th>
				<th style="width:70%">Feedback</th>
				<th>Date</th>
			  </tr>
			  
		<?php $feedbacks_array = $db_handle->runQuery("SELECT * FROM feedbacks"); 
		if (!empty($feedbacks_array)) { 
			foreach($feedbacks_array as $key=>$value){?>
				<tr>
					<td> <?php echo $feedbacks_array[$key]["id"]; ?> </td>
					<td> <?php echo $feedbacks_array[$key]["username"]; ?> </td>
					<td style="text-align:left"> <?php echo $feedbacks_array[$key]["feedback"]; ?> </td>
					<td> <?php echo $feedbacks_array[$key]["submitTime"]; ?> </td>
				</tr>
		<?php }
			}?>		
		</table>
	</div>
	</div>


</body>
</html>