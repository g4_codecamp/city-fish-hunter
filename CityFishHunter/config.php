<?php
class DBController {
	private $host = "localhost";
	private $user = "admin";
	private $password = "admin123";
	private $database = "cityfishhunter";
	private $conn;
	
	function __construct() {
		$this->conn = $this->connectDB();
	}
	
	function connectDB() {
		$conn = mysqli_connect($this->host,$this->user,$this->password,$this->database);
		if(!$conn){
			die(mysqli_error());
		}
		return $conn;
	}
	
	function addFeedback($userID,$username,$feedback){
		$now = date('Y/m/d h:i:sa');
		$query = $this->conn->prepare("INSERT INTO feedbacks(id, userID, username, feedback, submitTime) VALUES(null,?,?,?,?)");
		$query->bind_param("isss", $userID, $username, $feedback, $now);
		$query->execute();
	}
	
	
	function signup($username,$hash_password){
		$query = $this->conn->prepare("INSERT INTO users(PersonID, Username, Password, User_Role_ID) VALUES (null,?,?,2)");
		$query->bind_param("ss", $username, $hash_password);
		$query->execute();
	}
	
	function login($username,$hash_password){
		$query = "SELECT * FROM users where Username='".$username."'";
		$result = mysqli_query($this->conn,$query);
		$row = mysqli_fetch_assoc($result);
		return $row;
	}
	
	function runQuery($query) {
		$result = mysqli_query($this->conn,$query);
		while($row=mysqli_fetch_assoc($result)) {
			$resultset[] = $row;
		}		
		if(!empty($resultset))
			return $resultset;
	}
	
	function numRows($query) {
		$result  = mysqli_query($this->conn,$query);
		$rowcount = mysqli_num_rows($result);
		return $rowcount;	
	}
	
	function runQueryWithOutRs($query){
		mysqli_query($this->conn,$query);
	}
}
?>