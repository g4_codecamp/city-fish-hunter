<?php
	require_once 'config.php';
	$db_handle = new DBController();
	
	$user_role = $_SESSION['User_Role_ID'];
?>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>


<div class="w3-sidebar w3-bar-block w3-border-right" style="display:none" id="mySidebar">
  <button onclick="w3_close()" class="w3-bar-item w3-large">Close &times;</button>
  <a href="homepage.php" class="w3-bar-item w3-button">Home Page</a>
  <a href="shop.php" class="w3-bar-item w3-button">Shop</a>
  <a href="aboutus.php" class="w3-bar-item w3-button">About Us</a>
  <a href="checkOrder.php" class="w3-bar-item w3-button">Oder</a>
  <a href="feedbackpage.php" class="w3-bar-item w3-button">Give Feedback</a>
  <?php if($user_role == 1){
	echo '<a href="manageproduct.php" class="w3-bar-item w3-button">Manage Product</a>';
	echo '<a href="viewFeedbacks.php" class="w3-bar-item w3-button">Check Feedback</a>';
	}?>
  <a href="session_logout.php" class="w3-bar-item w3-button">Logout</a> 
</div>

<script>
function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
}

function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}
</script>
     
</body>
</html> 