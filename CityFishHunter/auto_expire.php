<?php

  if (isset($_SESSION['Username']))
  {
    $name = htmlspecialchars($_SESSION['Username']);
       
    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900)) {
        destroy_session_and_data();
		
		header('Location:index.php');
    }
    $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
  }

  function destroy_session_and_data()
{
   //session_start();
   //$_SESSION = array();
  
   unset($_SESSION['Username']);
   $_SESSION = array();
   session_unset();
   setcookie(session_name(), '', time() - 2592000, '/');
   session_destroy();
}

?>