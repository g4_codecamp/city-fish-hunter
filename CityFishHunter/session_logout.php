<?php 
  session_start();

  if (isset($_SESSION['Username']))
  {
    $name = htmlspecialchars($_SESSION['Username']);
    
    destroy_session_and_data();
    
    echo "Thank you " .$name.". ";
    echo "You have logged out successfully. <br>";
    echo "Please <a href=index.php>Click Here</a> to log in again.";
  }
  else echo "Please <a href='index.php'>Click Here</a> to log in.";
  
  function destroy_session_and_data()
{
   //session_start();
   //$_SESSION = array();
  
   unset($_SESSION['name']);
   $_SESSION = array();
   session_unset();
   setcookie(session_name(), '', time() - 2592000, '/');
   session_destroy();
}

?>