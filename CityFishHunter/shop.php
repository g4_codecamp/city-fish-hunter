<?php
	session_start();
	
	require_once 'auto_expire.php';
	require_once 'config.php';
	$db_handle = new DBController();
	require_once 'sidebar.php';
	
	if(!empty($_GET["action"])) {
		if(!empty($_POST["quantity"])) {
			$productByCode = $db_handle->runQuery("SELECT * FROM products WHERE ProductCode='" .$_GET['code']."'");
			$itemArray = array($productByCode[0]["ProductCode"]=>array('name'=>$productByCode[0]["ProductName"], 
                                     'code'=>$productByCode[0]["ProductCode"], 'quantity'=>$_POST["quantity"],
                                      'price'=>$productByCode[0]["Price"], 'image'=>$productByCode[0]["Path"]));
			
			if(!empty($_SESSION["cart_item"])) {
				
				if(in_array($productByCode[0]["ProductCode"],array_keys($_SESSION["cart_item"]))) {
					
					foreach($_SESSION["cart_item"] as $key => $value) {
                                                        
							if($productByCode[0]["ProductCode"] == $key) {
								
								if(empty($_SESSION["cart_item"][$key]["quantity"])) {
									$_SESSION["cart_item"][$key]["quantity"] = 0;
								}
								
								$_SESSION["cart_item"][$key]["quantity"] += $_POST["quantity"];
							}
					}
				}
                else {
					$_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
				}
			} else {
				$_SESSION["cart_item"] = $itemArray;
			}
		}
	}
?>


<!DOCTYPE html>
<html>
    <head>
        <title>CityFishHunter</title>
        <link rel="stylesheet" type="text/css" href="cityfishhunter.css">
    </head>
    <body>
        
            <div class="topnav">
                <div class="content">
					<a class="sidebar" onclick="w3_open()">☰</a>					
                    <a href="cart.php" style="float: right;">Cart</a>
                </div>
            </div>
            <div class="container">
                <div class="contentProduct">
				<h1>Fish</h1>
				<div class="grid-container">
				<?php
				$product_array = $db_handle->runQuery("SELECT * FROM products where Available='1' and Category='f'");
				if (!empty($product_array)) { 
					foreach($product_array as $key=>$value){
				?>
				
				
                <div class="grid-item">        
				<form method="post" action="shop.php?action=add&code=<?php echo $product_array[$key]["ProductCode"]; ?>">
				<div class="product-image"><img style="height:200px" src="<?php echo $product_array[$key]["Path"]; ?>"></div>
				
				<div class="product-title"><?php echo $product_array[$key]["ProductName"]; ?></div>
				<div class="product-price"><?php echo "RM".$product_array[$key]["Price"]; ?></div>
				<div class="cart-action"><input type="text" class="product-quantity" name="quantity" value="1" size="2" />     
                <input type="submit" value="Add to Cart" class="btnAddAction" /></div>
				
				</form>
				</div>
			
				<?php
					}
				}
				?>
				</div>
				<hr class="divider">
				<h1>Crab and Calm</h1>
				<div class="grid-container">
				<?php
				$product_array = $db_handle->runQuery("SELECT * FROM products where Available='1' and Category='c'");
				if (!empty($product_array)) { 
					foreach($product_array as $key=>$value){
				?>
				
				
                <div class="grid-item">        
				<form method="post" action="shop.php?action=add&code=<?php echo $product_array[$key]["ProductCode"]; ?>">
				<div class="product-image"><img style="height:200px" src="<?php echo $product_array[$key]["Path"]; ?>"></div>
				
				<div class="product-title"><?php echo $product_array[$key]["ProductName"]; ?></div>
				<div class="product-price"><?php echo "RM".$product_array[$key]["Price"]; ?></div>
				<div class="cart-action"><input type="text" class="product-quantity" name="quantity" value="1" size="2" />     
                <input type="submit" value="Add to Cart" class="btnAddAction" /></div>
				
				</form>
				</div>
			
				<?php
					}
				}
				?>
				</div>
				<hr class="divider">
				<h1>Sotong</h1>
				<div class="grid-container">
				<?php
				$product_array = $db_handle->runQuery("SELECT * FROM products where Available='1' and Category='s'");
				if (!empty($product_array)) { 
					foreach($product_array as $key=>$value){
				?>
				
				
                <div class="grid-item">        
				<form method="post" action="shop.php?action=add&code=<?php echo $product_array[$key]["ProductCode"]; ?>">
				<div class="product-image"><img style="height:200px" src="<?php echo $product_array[$key]["Path"]; ?>"></div>
				
				<div class="product-title"><?php echo $product_array[$key]["ProductName"]; ?></div>
				<div class="product-price"><?php echo "RM".$product_array[$key]["Price"]; ?></div>
				<div class="cart-action"><input type="text" class="product-quantity" name="quantity" value="1" size="2" />     
                <input type="submit" value="Add to Cart" class="btnAddAction" /></div>
				
				</form>
				</div>
			
				<?php
					}
				}
				?>
				</div>
				<hr class="divider">
                </div>
			</div>
        
    </body>
</html>