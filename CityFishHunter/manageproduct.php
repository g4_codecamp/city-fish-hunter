<?php
	session_start();
	
	require_once 'config.php';
	require_once 'auto_expire.php';
	$db_handle = new DBController();
	require_once 'sidebar.php';
	
	if(!empty($_GET["action"])) {
		if(!empty($_POST['price'])){
			$query = "UPDATE products SET Price='".$_POST['price']."' WHERE ProductCode='".$_GET['code']."'";
			$db_handle->runQueryWithOutRs($query);
		}
		if(!empty($_POST['availability'])){
			$query = "UPDATE products SET Available='".$_POST['availability']. "' WHERE ProductCode='".$_GET['code']."'";
			$db_handle->runQueryWithOutRs($query);
		}
	}
?>

<html>
    <head>
        <title>CityFishHunter</title>
        <link rel="stylesheet" type="text/css" href="cityfishhunter.css">
    </head>
    <body>
        
            <div class="topnav">
                <div class="content">
					<a class="sidebar" onclick="w3_open()">☰</a>					
                    <a href="cart.php" style="float: right;">Cart</a>
                </div>
            </div>
            <div class="container">
                <div class="contentProduct">
				<h1>Available Product</h1>
				<div class="grid-container">
				<?php
				$available_product_array = $db_handle->runQuery("SELECT * FROM products where Available='1'");
				if (!empty($available_product_array)) { 
					foreach($available_product_array as $key=>$value){
				?>
				
				
                <div class="grid-item">        
				<form method="post" action="manageproduct.php?action=change&code=<?php echo $available_product_array[$key]["ProductCode"]?>">
				<div class="product-image"><img style="height:200px" src="<?php echo $available_product_array[$key]["Path"]; ?>"></div>
				
				<div class="product-title"><?php echo $available_product_array[$key]["ProductName"]; ?></div>
				RM<input type="number" name="price" value=<?php echo $available_product_array[$key]["Price"]; ?> /><br>
				Availability<input type="number" name="availability" value=<?php echo $available_product_array[$key]["Available"]; ?> /><br><br>
                <input type="submit" value="Confirm change" class="btnAddAction" />
				
				</form>
				</div>
			
				<?php
					}
				}
				?>
				</div>
				<hr class="divider">
				<h1>Unavailable Product</h1>
				<div class="grid-container">
				<?php
				$unavailable_product_array = $db_handle->runQuery("SELECT * FROM products where NOT Available='1'");
				if (!empty($unavailable_product_array)) { 
					foreach($unavailable_product_array as $key=>$value){
				?>
				
				
                <div class="grid-item">        
				<form method="post" action="manageproduct.php?action=change&code=<?php echo $unavailable_product_array[$key]["ProductCode"]?>">
				<div class="product-image"><img style="height:200px" src="<?php echo $unavailable_product_array[$key]["Path"]; ?>"></div>
				
				<div class="product-title"><?php echo $unavailable_product_array[$key]["ProductName"]; ?></div>
				RM<input type="number" name="price" value=<?php echo $unavailable_product_array[$key]["Price"]; ?> /><br>
				Availability<input name="availability" type="number" value=<?php echo $unavailable_product_array[$key]["Available"]; ?> /><br><br>
                <input type="submit" value="Confirm change"/>
				
				</form>
				</div>
			
				<?php
					}
				}
				?>
				</div>
                </div>
			</div>
        
    </body>
</html>