<?php
	session_start();
	
	require_once 'sidebar.php';
	require_once 'auto_expire.php';
	
	if(!empty($_SESSION["cart_item"])){
		$cart_item = $_SESSION['cart_item'];
	}
	
	if(!empty($_GET["action"])){
		if(!empty($_SESSION["cart_item"])) {
			foreach($_SESSION["cart_item"] as $k => $v) {
				if($_GET["code"] == $k){
					unset($_SESSION["cart_item"][$k]);
					$cart_item = $_SESSION['cart_item'];
				}
				if(empty($_SESSION["cart_item"])){
					unset($_SESSION["cart_item"]);
				}
			}
			
		}
	}
	
	if(!empty($_POST['submit'])){
		$query = "INSERT INTO orders VALUES (null," .$_SESSION['PersonID'].",'".serialize($cart_item)."',".$_SESSION['total_price'].",'".date('Y/m/d h:i:sa')."')";
		$db_handle->runQueryWithOutRs($query);
		unset($_SESSION["cart_item"]);
		$cart_item = [];
	}
?>

<html>
<link rel="stylesheet" type="text/css" href="cityfishhunter.css">
<body>
	<div class="topnav">
        <div class="content">
			<a class="sidebar" onclick="w3_open()">☰</a>					
            <a href="cart.php" style="float: right;">Cart</a>
        </div>
    </div>

	<div class="cart">
	<h2>Shopping Cart</h2>
	<?php
		if(isset($_SESSION["cart_item"])){
			$total_quantity = 0;
			$total_price = 0;
		?>
		<table  cellpadding="10" cellspacing="1">
		<tbody>
		<tr>
		<th style="text-align:left; width:50%">Name</th>
		<th style="text-align:right;" >Quantity</th>
		<th style="text-align:right;" >Unit Price</th>
		<th style="text-align:right;" >Price</th>
		<th style="text-align:center;" >Remove</th>
		</tr>	
		<?php		
			foreach ($cart_item as $item){
				$item_price = $item["quantity"]*$item["price"];
				?>
						<tr>
						<td><img style="height:150px" src="<?php echo $item["image"]; ?>" class="cart-item-image" /><?php echo $item["name"]; ?></td>
						<td style="text-align:right;"><?php echo $item["quantity"]; ?></td>
						<td  style="text-align:right;"><?php echo "RM ".$item["price"]; ?></td>
						<td  style="text-align:right;"><?php echo "RM ". number_format($item_price,2); ?></td>
						<td style="text-align:center;">
										<!-- Get method to remove item in Cart -->
										<a href="cart.php?action=remove&code=<?php echo $item["code"]; ?>" class="btnRemoveAction">
										<img src="icon-delete.png" alt="Remove Item" /></a></td>
						</tr>
						<?php
						$total_quantity += $item["quantity"];
						$total_price += ($item["price"]*$item["quantity"]);
						$_SESSION['total_price'] = $total_price;
				}
				?>

		<tr>
		<td colspan="2" align="right">Total:</td>
		<td align="right"><?php echo $total_quantity; ?></td>
		<td align="right" colspan="2"><strong><?php echo "RM ".number_format($total_price, 2); ?></strong></td>
		<td></td>
		</tr>
		</tbody>
		</table>

		<form action="cart.php" method="post">
			<input type="submit" name="submit" value="Check Out">
		</form>
		
		  <?php
		}else {
		?>
		<!-- When cart is empty  -->
		<div class="no-records">Your Cart is Empty</div>
		<?php 
		}
		?>
	</div>
</body>
</html>
	